#ifndef TACKA_H
#define TACKA_H


class Tacka
{
public:
     Tacka(double x1, double x2, double x3 );
     static Tacka nevidljiva_tacka(Tacka p1, Tacka p2, Tacka p3, Tacka p5, Tacka p6, Tacka p7, Tacka p8);
     double x1,x2,x3;
     static Tacka vektorski_proizvod (Tacka A, Tacka B);
     static Tacka srednja_vrednost (Tacka t1, Tacka t2, Tacka t3);
    //static void nevidljiva_tacka(Tacka p1, Tacka p2, Tacka p3, Tacka p5, Tacka p6, Tacka p7, Tacka p8);
   // double x, y;
};

#endif // TACKA_H
