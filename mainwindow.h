#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "QImage"
#include "QFileDialog"
#include "QPixmap"
#include "QGraphicsScene"
#include <QMainWindow>
#include "QMouseEvent"
#include "tacka.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void mousePressEvent(QMouseEvent *event);


    //QImage image;
    QGraphicsScene *scene;
    QPixmap image;
    int count =1;
    double p1x,p1y,p2x,p2y,p3x,p3y,p5x,p5y,p6x,p6y,p7x,p7y,p8x,p8y;
   //acka p1,p2,p3,p4,p5,p6,p7,p8;

private slots:
    void ucitajSliku();
    void izracunaj();

private:
    Ui::MainWindow *ui;
    void prikazi();
};
#endif // MAINWINDOW_H
