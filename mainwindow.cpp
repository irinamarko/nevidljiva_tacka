#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QLabel"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pbUcitaj, &QPushButton::clicked, this, &MainWindow::ucitajSliku);
    ui->pbizracunaj->setEnabled(false);
    connect(ui->pbizracunaj, &QPushButton::clicked,this, &MainWindow::izracunaj);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{



    switch (count) {
    case 1:
        ui->lp1->setText("P1  X:"+ QString::number(event->x())+"Y:"+ QString::number(event->y()));
        p1x=event->x();
        p1y=event->y();
        count++;
        break;

    case 2:
        ui->lp2->setText("P2  X:"+ QString::number(event->x())+"Y:"+ QString::number(event->y()));
        p2x=event->x();
        p2y=event->y();
        count++;
        break;
    case 3:
        ui->lp3->setText("P3  X:"+ QString::number(event->x())+"Y:"+ QString::number(event->y()));
        p3x=event->x();
        p3y=event->y();
        count++;
        count++;
        break;
    case 5:
        ui->lp5->setText("P5  X:"+ QString::number(event->x())+"Y:"+ QString::number(event->y()));
        p5x=event->x();
        p5y=event->y();
        count++;
        break;
    case 6:
        ui->lp6->setText("P6  X:"+ QString::number(event->x())+"Y:"+ QString::number(event->y()));
        p6x=event->x();
        p6y=event->y();
        count++;
        break;
    case 7:
        ui->lp7->setText("P7  X:"+ QString::number(event->x())+"Y:"+ QString::number(event->y()));
        p7x=event->x();
        p7y=event->y();
        count++;
        break;
    case 8:
        ui->lp8->setText("P8  X:"+ QString::number(event->x())+"Y:"+ QString::number(event->y()));
        p8x=event->x();
        p8y=event->y();
        count++;
        ui->pbizracunaj->setEnabled(true);
        break;


    }





}





void MainWindow::ucitajSliku()
{
    ui->pbizracunaj->setEnabled(false);
    count = 1;
    QString filter = QString("Supported Files (*.shp *.kml *.jpg *.png );;All files (*)");
    QString fileName = QFileDialog::getOpenFileName(this, tr("Select File(s)"), QDir::homePath(), filter);
   // QImage image ;
    bool success = image.load(fileName);
    qDebug() << "File loaded succesfully " << success ;

    if(success){

        prikazi();
    }
}

void MainWindow::izracunaj()
{
   Tacka p1(p1x,p1y,1);
   Tacka p2(p2x,p2y,1);
   Tacka p3(p3x,p3y,1);
   Tacka p5(p5x,p5y,1);
   Tacka p6(p6x,p6y,1);
   Tacka p7(p7x,p7y,1);
   Tacka p8(p8x,p8y,1);

   Tacka p4 =Tacka::nevidljiva_tacka(p1, p2,  p3,  p5, p6, p7, p8);

    ui->lp4->setText("P4  X:"+ QString::number(p4.x1/p4.x3)+"    "+"Y:"+ QString::number(p4.x2/p4.x3));

}

void MainWindow::prikazi()
{
    scene = new QGraphicsScene(this);
    image.scaledToHeight(1000);
    image.scaledToWidth(800);
    scene->addPixmap(image);
    scene->setSceneRect(image.rect());
    ui->mainImage->setScene(scene);
}

