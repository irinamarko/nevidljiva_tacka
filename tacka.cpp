#include "tacka.h"
#include "iostream"


/*
Tacka::Tacka(double x1, double x2, double x3)
    :x1(x1),
      x2(x2),
      x3(x3)
{}
*/

Tacka::Tacka(double x1, double x2, double x3){
    this->x1=x1;
    this->x2=x2;
    this->x3=x3;
}

Tacka Tacka::vektorski_proizvod(Tacka A, Tacka B)
{

     double x1 =  A.x2 * B.x3 - A.x3 * B.x2;
     double x2 =  A.x3 * B.x1 - A.x1 * B.x3;
     double x3 =  A.x1 * B.x2 - A.x2 * B.x1;
     Tacka C(x1,x2,x3);
     return C;
}

Tacka Tacka::srednja_vrednost (Tacka t1, Tacka t2, Tacka t3){

     double x1 = t1.x1 + t2.x1 + t3.x1;
     double x2 = t1.x2 + t2.x2 + t3.x2;
     double x3 = t1.x3 + t2.x3 + t3.x3;

     x1 = x1/3;
     x2 = x2/3;
     x3 = x3/3;
     Tacka t(x1,x2,x3);
     return t;
}

Tacka Tacka::nevidljiva_tacka(Tacka p1, Tacka p2, Tacka p3, Tacka p5, Tacka p6, Tacka p7, Tacka p8)
{


        Tacka p26 = vektorski_proizvod(p2,p6);
        Tacka p15 = vektorski_proizvod(p1,p5);
        Tacka p37 = vektorski_proizvod(p3,p7);

        Tacka Xb0 = vektorski_proizvod(p26, p15);
        Tacka Xb1 = vektorski_proizvod(p26, p37);
        Tacka Xb2 = vektorski_proizvod(p15, p37);

        Tacka Xb = srednja_vrednost(Xb0, Xb1, Xb2);

        Tacka p78 = vektorski_proizvod(p7,p8);
        Tacka p56 = vektorski_proizvod(p5,p6);
        Tacka Yb = vektorski_proizvod(p56, p78);

        Tacka Xb8 = vektorski_proizvod(Xb, p8);
        Tacka Yb3 = vektorski_proizvod(Yb, p3);
        Tacka p4= vektorski_proizvod (Xb8, Yb3);



        std::cout << "(" << p4.x1/p4.x3 << ", " << p4.x2/p4.x3 << ")" << std::endl;
        return p4;


}

